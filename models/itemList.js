class ItemList {
  constructor() {
    this.arr = [];
  }
  classifyItems(items) {
    this.arr.push(items);
  }
  renderItems() {
    let content = "";
    content = this.arr.reduce((itemContent, item, index) => {
      itemContent += `
        <div class="col-3 my-5">
            <img
                class="img-fluid"
                src="${item.imgSrc_jpg}"
                alt=""
            />
            <button onclick="tryOn(event)" data-type="${item.type}" data-index="${index}" class="btn btn-success">Try on</button>
        </div>
        `;
      return itemContent;
    }, "");
    return content;
  }
}
export default ItemList;

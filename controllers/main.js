let dataItems = [
  {
    id: "topcloth_1",
    type: "topclothes",
    name: "Colorful Blouse",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/topcloth1_show.jpg",
    imgSrc_png: "../assets/images/clothes/topcloth1.png",
  },
  {
    id: "topcloth_2",
    type: "topclothes",
    name: "Green Blouse",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/topcloth2_show.jpg",
    imgSrc_png: "../assets/images/clothes/topcloth2.png",
  },
  {
    id: "topcloth_3",
    type: "topclothes",
    name: "Ocean Blouse",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/topcloth3_show.jpg",
    imgSrc_png: "../assets/images/clothes/topcloth3.png",
  },
  {
    id: "topcloth_4",
    type: "topclothes",
    name: "Gradient Pink Shirt",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/topcloth4_show.jpg",
    imgSrc_png: "../assets/images/clothes/topcloth4.png",
  },
  {
    id: "topcloth_5",
    type: "topclothes",
    name: "Modern Denim Shirt",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/topcloth5_show.jpg",
    imgSrc_png: "../assets/images/clothes/topcloth5.png",
  },
  {
    id: "topcloth_6",
    type: "topclothes",
    name: "Orange Juicy Blouse",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/topcloth6_show.jpg",
    imgSrc_png: "../assets/images/clothes/topcloth6.png",
  },
  {
    id: "botcloth_1",
    type: "botclothes",
    name: "Bot Cloth 1",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/botcloth1_show.jpg",
    imgSrc_png: "../assets/images/clothes/botcloth1.png",
  },
  {
    id: "botcloth_2",
    type: "botclothes",
    name: "Bot Cloth 2",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/botcloth2_show.jpg",
    imgSrc_png: "../assets/images/clothes/botcloth2.png",
  },
  {
    id: "botcloth_3",
    type: "botclothes",
    name: "Bot Cloth 3",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/botcloth3_show.jpg",
    imgSrc_png: "../assets/images/clothes/botcloth3.png",
  },
  {
    id: "botcloth_4",
    type: "botclothes",
    name: "Bot Cloth 4",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/botcloth4_show.jpg",
    imgSrc_png: "../assets/images/clothes/botcloth4.png",
  },
  {
    id: "botcloth_5",
    type: "botclothes",
    name: "Bot Cloth 5",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/clothes/botcloth5_show.jpg",
    imgSrc_png: "../assets/images/clothes/botcloth5.png",
  },
  {
    id: "shoes_1",
    type: "shoes",
    name: "Shoes 1",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/shoes/shoes1_show.jpg",
    imgSrc_png: "../assets/images/shoes/shoes1.png",
  },
  {
    id: "shoes_2",
    type: "shoes",
    name: "Shoes 2",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/shoes/shoes2_show.jpg",
    imgSrc_png: "../assets/images/shoes/shoes2.png",
  },
  {
    id: "shoes_3",
    type: "shoes",
    name: "Shoes 3",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/shoes/shoes3_show.jpg",
    imgSrc_png: "../assets/images/shoes/shoes3.png",
  },
  {
    id: "shoes_4",
    type: "shoes",
    name: "Shoes 4",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/shoes/shoes4_show.jpg",
    imgSrc_png: "../assets/images/shoes/shoes4.png",
  },
  {
    id: "shoes_5",
    type: "shoes",
    name: "Shoes 5",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/shoes/shoes5_show.jpg",
    imgSrc_png: "../assets/images/shoes/shoes5.png",
  },
  {
    id: "handbag_1",
    type: "handbags",
    name: "Handbag 1",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/handbags/handbag1_show.jpg",
    imgSrc_png: "../assets/images/handbags/handbag1.png",
  },
  {
    id: "handbag_2",
    type: "handbags",
    name: "Handbag 2",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/handbags/handbag2_show.jpg",
    imgSrc_png: "../assets/images/handbags/handbag2.png",
  },
  {
    id: "handbag_3",
    type: "handbags",
    name: "Handbag 3",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/handbags/handbag3_show.jpg",
    imgSrc_png: "../assets/images/handbags/handbag3.png",
  },
  {
    id: "necklace_1",
    type: "necklaces",
    name: "Necklace 1",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/necklaces/necklace1_show.jpg",
    imgSrc_png: "../assets/images/necklaces/necklace1.png",
  },
  {
    id: "necklace_2",
    type: "necklaces",
    name: "Necklace 2",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/necklaces/necklace2_show.jpg",
    imgSrc_png: "../assets/images/necklaces/necklace2.png",
  },
  {
    id: "necklace_3",
    type: "necklaces",
    name: "Necklace 3",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/necklaces/necklace3_show.jpg",
    imgSrc_png: "../assets/images/necklaces/necklace3.png",
  },
  {
    id: "hairstyle_1",
    type: "hairstyle",
    name: "Hairstyle 1",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/hairstyle/hairstyle1_show.jpg",
    imgSrc_png: "../assets/images/hairstyle/hairstyle1.png",
  },
  {
    id: "hairstyle_2",
    type: "hairstyle",
    name: "Hairstyle 2",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/hairstyle/hairstyle2_show.jpg",
    imgSrc_png: "../assets/images/hairstyle/hairstyle2.png",
  },
  {
    id: "hairstyle_3",
    type: "hairstyle",
    name: "Hairstyle 3",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/hairstyle/hairstyle3_show.jpg",
    imgSrc_png: "../assets/images/hairstyle/hairstyle3.png",
  },
  {
    id: "background_1",
    type: "background",
    name: "Background 1",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/background/background1_show.jpg",
    imgSrc_png: "../assets/images/background/background1.jpg",
  },
  {
    id: "background_2",
    type: "background",
    name: "Background 2",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/background/background2_show.jpg",
    imgSrc_png: "../assets/images/background/background2.jpg",
  },
  {
    id: "background_3",
    type: "background",
    name: "Background 3",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/background/background3_show.jpg",
    imgSrc_png: "../assets/images/background/background3.jpg",
  },
  {
    id: "background_4",
    type: "background",
    name: "Background 4",
    desc:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid, nulla.",
    imgSrc_jpg: "../assets/images/background/background4_show.jpg",
    imgSrc_png: "../assets/images/background/background4.jpg",
  },
];
import ItemList from "../models/itemList.js";
const upperBodyItems = new ItemList();
const lowerBodyItems = new ItemList();
const footwearItems = new ItemList();
const bagItems = new ItemList();
const necklaceItems = new ItemList();
const hairstyles = new ItemList();
const background = new ItemList();

const showItem = () => {
  for (let value of dataItems) {
    if (value.type === "topclothes") {
      upperBodyItems.classifyItems(value);
    } else if (value.type === "botclothes") {
      lowerBodyItems.classifyItems(value);
    } else if (value.type === "shoes") {
      footwearItems.classifyItems(value);
    } else if (value.type === "handbags") {
      bagItems.classifyItems(value);
    } else if (value.type === "necklaces") {
      necklaceItems.classifyItems(value);
    } else if (value.type === "hairstyle") {
      hairstyles.classifyItems(value);
    } else if (value.type === "background") {
      background.classifyItems(value);
    }
  }
  console.log(necklaceItems);
};
showItem();

document.getElementById("pills-upper").addEventListener("click", () => {
  const display = upperBodyItems.renderItems();
  document.getElementById("showItem").innerHTML = display;
});

document.getElementById("pills-lower").addEventListener("click", () => {
  const display = lowerBodyItems.renderItems();
  document.getElementById("showItem").innerHTML = display;
});
document.getElementById("pills-footwear").addEventListener("click", () => {
  const display = footwearItems.renderItems();
  document.getElementById("showItem").innerHTML = display;
});
document.getElementById("pills-bags").addEventListener("click", () => {
  const display = bagItems.renderItems();
  document.getElementById("showItem").innerHTML = display;
});
document.getElementById("pills-necklaces").addEventListener("click", () => {
  const display = necklaceItems.renderItems();
  document.getElementById("showItem").innerHTML = display;
});
document.getElementById("pills-hairstyle").addEventListener("click", () => {
  const display = hairstyles.renderItems();
  document.getElementById("showItem").innerHTML = display;
});
document.getElementById("pills-background").addEventListener("click", () => {
  const display = background.renderItems();
  document.getElementById("showItem").innerHTML = display;
});

const tryOn = (event) => {
  const index = event.currentTarget.getAttribute("data-index");
  const type = event.currentTarget.getAttribute("data-type");
  if (type === "topclothes") {
    document.getElementById("upper").innerHTML = `
      <img class="img-fluid" src="${upperBodyItems.arr[index].imgSrc_png}"/>
    `;
  } else if (type === "botclothes") {
    document.getElementById("lower").innerHTML = `
      <img class="img-fluid" src="${lowerBodyItems.arr[index].imgSrc_png}"/>
    `;
  } else if (type === "shoes") {
    document.getElementById("feet").innerHTML = `
      <img class="img-fluid" src="${footwearItems.arr[index].imgSrc_png}"/>
    `;
  } else if (type === "handbags") {
    document.getElementById("handbag").innerHTML = `
      <img class="img-fluid" src="${bagItems.arr[index].imgSrc_png}"/>
    `;
  } else if (type === "necklaces") {
    document.getElementById("necklace").innerHTML = `
      <img class="img-fluid" src="${necklaceItems.arr[index].imgSrc_png}"/>
    `;
  } else if (type === "hairstyle") {
    document.getElementById("hairstyle").innerHTML = `
      <img class="img-fluid" src="${hairstyles.arr[index].imgSrc_png}"/>
    `;
  } else if (type === "background") {
    document.getElementById("background").innerHTML = `
      <img class="img-fluid w-100" src="${background.arr[index].imgSrc_png}"/>
    `;
  }
};
window.tryOn = tryOn;
